# Docker setup for {{ cookiecutter.project_slug }}

{% if cookiecutter.project_short_description %}
{{ cookiecutter.project_short_description }}
{% endif %}

This repository contains the files that are necessary to run and build the
images for the {{ cookiecutter.project_slug }} project.

## Installation

You need to have [docker](https://docs.docker.com/get-docker/) installed and
you need to clone this repository. We also recommend that you install
[`docker compose`](https://docs.docker.com/compose/install/).

Once you did this, create a folder named `{{ cookiecutter.project_slug }}` and
clone the `docker` repository:

```bash
# clone the repository
git clone https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/{{ cookiecutter.project_slug }}.git
# change into the cloned repository
cd {{ cookiecutter.project_slug }}
```
Now you need to build and deploy the images. We recommend that you do this via
`docker compose` (see below).

## Using openshift

We have a dedicated helm chart for this docker setup, accessible from
https://codebase.helmholtz.cloud/hcdc/kubernetes/charts/thredds

If you deploy this setup using this chart and made changes to the source code
of this repository, you need to run

```bash
oc start-build thredds
```

to trigger a new build.

## Using docker compose

The recommended way to build and deploy the files in this repository is to
use `docker compose`. You can install this manually for your operating system
(see the [docker docs](https://docs.docker.com/compose/install/)).

### Building the images

You first need to build the images locally via

```bash
docker compose build
```

### Running the containers

To run the containers, simply do a

```bash
docker compose up
```

or if you want to detach it, run

```bash
docker compose up -d
```

## Building the image manually

If you do not want to use `docker compose`, you can also build the image
manually via

```bash
docker build -t {{ cookiecutter.project_slug }}_thredds -f docker/thredds/Dockerfile .
```

## Technical note

This package has been generated from the template
{{ cookiecutter._template }}.

See the template repository for instructions on how to update the skeleton for
this package.


## About this repository

### Authors:
{%- for author_info in get_author_infos(cookiecutter.project_authors.split(','), cookiecutter.project_author_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}


### Maintainers
{%- for author_info in get_author_infos(cookiecutter.project_maintainers.split(','), cookiecutter.project_maintainer_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}

## License information

Copyright © {{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}

{% if cookiecutter.use_reuse == "yes" %}
Code files in this repository is licensed under the {{ cookiecutter.code_license }}.

Documentation files in this repository is licensed under {{ cookiecutter.documentation_license }}.

Supplementary and configuration files in this repository are licensed
under {{ cookiecutter.supplementary_files_license }}.

Please check the header of the individual files for more detailed
information.

{% else %}
All rights reserved.
{% endif %}